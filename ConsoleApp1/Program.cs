﻿using ConsoleApp1.Models;

Gadiformes gadiformes = new Gadiformes("Gadiformes", HierarhyType.order);
Console.WriteLine(gadiformes.ToString());
Gadiformes gadiformesCopy = gadiformes.MyClone();
Console.WriteLine(gadiformesCopy.ToString());

Osteichthyes osteichthyes = new Osteichthyes("Osteichthyes", HierarhyType.superclass);
Console.WriteLine(osteichthyes.ToString());
Osteichthyes osteichthyesCopy = (Osteichthyes)osteichthyes.Clone();
Console.WriteLine(osteichthyesCopy.ToString());