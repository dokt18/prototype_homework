﻿using ConsoleApp1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Models
{
    public class Chordate: IMyClonable<Chordate>, ICloneable
    {
        public string Name { get; set; }
        public HierarhyType HierarhyType { get; set; }
        public Chordate(string name, HierarhyType hierarhyType)
        {
            Name = name;
            HierarhyType = hierarhyType;
        }

        public virtual object Clone()
        {
            return new Chordate(Name, HierarhyType);
        }

        public virtual Chordate MyClone()
        {
            return new Chordate(Name, HierarhyType);
        }
        public override string ToString()
        {
            return $"{Name} - {HierarhyType}";
        }
    }
}
