﻿using ConsoleApp1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Models
{
    public class Osteichthyes: Chordate,IMyClonable<Osteichthyes>, ICloneable
    {
        public Osteichthyes(string name, HierarhyType type) : base(name, type)
        {

        }
        public override Osteichthyes MyClone()
        {
            return new Osteichthyes(Name, HierarhyType);
        }
        public override object Clone()
        {
            return new Osteichthyes(Name, HierarhyType);
        }
    }
}
