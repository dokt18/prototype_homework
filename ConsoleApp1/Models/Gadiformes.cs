﻿using ConsoleApp1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Models
{
    public class Gadiformes : Osteichthyes, IMyClonable<Gadiformes>, ICloneable
    {
        public Gadiformes(string name, HierarhyType type) : base(name, type)
        {
        }
        public override Gadiformes MyClone()
        {
            return new Gadiformes(Name, HierarhyType);
        }
        public override object Clone()
        {
            return new Gadiformes(Name, HierarhyType);
        }
    }
}
