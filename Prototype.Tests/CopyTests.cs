using ConsoleApp1.Models;
using Newtonsoft.Json;
using Xunit;

namespace Prototype.Tests
{
    public class CopyTests
    {
        [Fact]
        public void CanCopyObjectWithMyClonable()
        {
            Osteichthyes osteichthyes = new Osteichthyes("Test", HierarhyType.order);
            Osteichthyes osteichthyesCopy = osteichthyes.MyClone();
            
            var osteichthyesJson = JsonConvert.SerializeObject(osteichthyes);
            var osteichthyesCopyJson = JsonConvert.SerializeObject(osteichthyesCopy);

            Assert.Equal(osteichthyesJson, osteichthyesCopyJson);
        }

        [Fact]
        public void CanCopyObjectWithIConable()
        {
            Osteichthyes osteichthyes = new Osteichthyes("Test", HierarhyType.order);
            Osteichthyes osteichthyesCopy = (Osteichthyes)osteichthyes.Clone();

            var osteichthyesJson = JsonConvert.SerializeObject(osteichthyes);
            var osteichthyesCopyJson = JsonConvert.SerializeObject(osteichthyesCopy);

            Assert.Equal(osteichthyesJson, osteichthyesCopyJson);
        }
    }
}